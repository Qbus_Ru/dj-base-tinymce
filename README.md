# dj-base-tinymce

Base for dev/prod django project with docker deployment and tinymce editor supply. 

Django + postgres + gunicorn + nginx + docker-compose


## Check-list

init errands:
* copy `.gitignore` file to root directory from  `samples` directory
* copy .env to root directory from `samples` directory
* copy .env.db to root directory from `samples` directory
* make secret key with `secret.sh` and copy secret key to .env
* by the same script make password db and add it to .env & .env.db 
* than delete `samples` directory

setup todo:
* add new db-name to .env & .env.db 
* change static volume's name in docker-compose.yml & dev.yml files
* change db volume's name in docker-compose.yml & dev.yml files

To run /admin make the exec with docker-compose after docker-compose up: exec web ./manage.sh 