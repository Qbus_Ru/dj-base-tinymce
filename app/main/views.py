from django.shortcuts import render, redirect
# from django.contrib import messages
# from .models import


def index(request):
    return render(request=request, template_name='main/index.html', context={'hello': 'Hello, everybody!'})
